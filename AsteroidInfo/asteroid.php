<?php
use Phpfastcache\Helper\Psr16Adapter;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

require __DIR__.'/../config.php';

function getMessage()
{
    $data = getInfo();

    if (count($data) == 0) {
        return "Não, fique tranquilo ;-)";
    }

    return formatMessage($data);
}

function formatMessage($data)
{
    $str = "Sim!";
    
    foreach($data as $a){
        $str = $str . "\n\nNome: " . $a['name'];
        
        $str = $str . "\n" . "Diâmetro estimado entre " . number_format($a['diameter_min'], 2, ',', '.') . " a " . number_format($a['diameter_max'], 2, ',', '.') . " km.";
        $str = $str . "\n" . "Velocidade relativa à Terra: " . number_format($a['relative_velocity'], 0, ',','.') . " km/h.";
        $str = $str . "\n" . "Menor distância entre a Terra: " . number_format($a['miss_distance'], 0, ',','.') . " km.";
        $str = $str . "\n" . "Saiba mais em: " . $a['link'];
    }

    return $str;
}

function getInfo()
{  
    global $cache_path;
    $Psr16Adapter = new Psr16Adapter("Files", ["path" => $cache_path]);
    $date = date("Y-m-d");

    if(!$Psr16Adapter->has($date)){
        $data = getHazardousAsteroidsInfo();
        $Psr16Adapter->set($date, $data, 24*60*60);
    }else{
        $data = $Psr16Adapter->get($date);
    }

    return $data;
}

function getHazardousAsteroidsInfo()
{
    $r = fetchDataFromAPI();

    $array = array();

    foreach ($r->near_earth_objects as $d){
        foreach ($d as $a){
            if ($a->is_potentially_hazardous_asteroid == true){
                $n = [
                    "name" => $a->name,
                    "diameter_min" => $a->estimated_diameter->kilometers->estimated_diameter_min,
                    "diameter_max" => $a->estimated_diameter->kilometers->estimated_diameter_max,
                    "miss_distance" => $a->close_approach_data[0]->miss_distance->kilometers,
                    "relative_velocity" => $a->close_approach_data[0]->relative_velocity->kilometers_per_hour,
                    "link" => $a->nasa_jpl_url
                ];

                array_push($array, $n);
            }
        }
    }

    return $array;
}

function fetchDataFromAPI()
{
    $client = new GuzzleHttp\Client();

    $res = $client->request('GET', buildUrlCall(),
    [
        'headers' => [
        'Accept' => 'application/json',
        'Content-type' => 'application/json'
    ]]);

    $r = json_decode($res->getBody()->getContents());
    
    return $r;
}

function buildUrlCall(){
    $date = date("Y-m-d");
    global $api_token;

    return "https://api.nasa.gov/neo/rest/v1/feed?start_date=" . $date . "&end_date=" . $date . "&detailed=false&api_key=" . $api_token;
}

?>