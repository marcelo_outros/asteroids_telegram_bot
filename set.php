<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

$bot_api_key  = $token;
$bot_username = $username;
$hook_url     = $base_url . 'hook.php';

try {
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

    $result = $telegram->setWebhook($hook_url);
    if ($result->isOk()) {
        echo $result->getDescription();
    }
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e->getMessage();
}