<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

$bot_api_key  = $token;
$bot_username = $username;
try {
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);
    $result = $telegram->deleteWebhook();
    if ($result->isOk()) {
        echo $result->getDescription();
    }
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e->getMessage();
}

?>