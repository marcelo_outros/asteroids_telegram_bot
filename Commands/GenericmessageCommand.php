<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Commands\SystemCommand;

require_once('AsteroidInfo/asteroid.php');

class GenericmessageCommand extends SystemCommand
{

    public function execute()
    {
        $message = $this->getMessage();

        $q = "Algum asteroide oferece perigo ao planeta Terra na data de hoje?";
        if (strcmp($q, $message->getText()) == 0){    
            return Request::sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text'    => getMessage()
            ]);
        }

        return Request::emptyResponse();
    }

}
